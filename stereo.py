import cv2
import numpy as np
from matplotlib import pyplot as plt


def load_gray_image(path):
    return cv2.cvtColor(cv2.imread(str(path)), cv2.COLOR_RGB2GRAY)

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

def objectpoints_for_chessboard(size):
    objp = np.zeros((1, size[0] * size[1], 3), np.float32)
    objp[0,:,:2] = np.mgrid[0:size[0], 0:size[1]].T.reshape(-1, 2)
    return objp

def imgpoints_for_chessboard(img, chessboard_size):
    ret, corners = cv2.findChessboardCorners(img, chessboard_size)
    if ret:
        subcorners = cv2.cornerSubPix(img, corners, winSize=(11, 11), zeroZone=(-1, -1), criteria=criteria)
        return subcorners
    else:
        return None

def show_chessboard_corners(img, corners, chessboard_size):
    out = img.copy()
    cv2.drawChessboardCorners(out, chessboard_size, corners, True)
    plt.figure(figsize=(15, 15))
    plt.imshow(out, cmap='gray')
    plt.show()
    
def skel(img):
    element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
    skel = np.zeros(img.shape,np.uint8)

    size = np.size(img)
    zeros = size - cv2.countNonZero(img)

    while zeros!=size:
        eroded = cv2.erode(img,element)
        temp = cv2.dilate(eroded,element)
        temp = cv2.subtract(img,temp)
        skel = cv2.bitwise_or(skel,temp)
        img = eroded
        zeros = size - cv2.countNonZero(img)

    return skel
